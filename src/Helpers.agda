{-# OPTIONS -v test:2 #-}

module Helpers where

open import Prelude.Function
open import Prelude.List
open import Prelude.Bool
open import Prelude.Nat renaming (Nat to ℕ ; _+N_ to _+_)
open import Tactic.Reflection renaming (bindTC to _>>=_ ; unify to unifyTC)
open import Tactic.Reflection.Free

occursN : ℕ → Term → Bool
occursN n t = any? (eqNat n) (freeVars t)

occurs0 : Term → Bool
occurs0 = occursN 0

data Op : Set where
  Suc Pred : Op

private
  pred : ℕ → ℕ
  pred 0 = 0
  pred (suc n) = n

  _∙_ : Op → ℕ → ℕ
  Suc ∙ n = suc n
  Pred ∙ n = pred n

  geNat : ℕ → ℕ → Bool
  geNat 0 0 = true
  geNat 0 (suc _) = false
  geNat (suc _) 0 = true
  geNat (suc n) (suc m) = geNat n m

  updateArgs : ℕ → Op → List $ Arg Term → List $ Arg Term
  updateClause : ℕ → Op → List Clause → List Clause
  updateSort : ℕ → Op → Sort → Sort
  updateTerm' : ℕ → Op → Term → Term

  updateTerm' n op (var x args) = if geNat x n then var (op ∙ x) (updateArgs n op args) else var x (updateArgs n op args)
  updateTerm' n op (con c args) = con c $ updateArgs n op args
  updateTerm' n op (def f args) = def f $ updateArgs n op args
  updateTerm' n op (lam v (abs s x)) = lam v $ abs s $ updateTerm' (suc n) op x
  updateTerm' n op (pat-lam cs args) = pat-lam (updateClause (suc n) op cs) (updateArgs (suc n) op args)
  updateTerm' n op (pi (arg i x) (abs s y)) = pi (arg i $ updateTerm' n op x) (abs s $ updateTerm' n op y)
  updateTerm' n op (agda-sort s) = agda-sort $ updateSort n op s
  updateTerm' n op (lit l) = lit l
  updateTerm' n op (meta x args) = meta x $ updateArgs n op args
  updateTerm' n op unknown = unknown

  updateArgs n op [] = []
  updateArgs n op (arg i x ∷ args) = arg i (updateTerm' n op x) ∷ updateArgs n op args

  updateClause n op [] = []
  updateClause n op (clause ps t ∷ cs) = clause ps (updateTerm' n op t) ∷ updateClause n op cs
  updateClause n op (absurd-clause ps ∷ cs) = absurd-clause ps ∷ updateClause n op cs

  updateSort n op (set t) = set $ updateTerm' n op t
  updateSort n op (lit l) = lit l
  updateSort n op unknown = unknown

updateTerm : Op → Term → Term
updateTerm = updateTerm' 0

ArgTypes2Terms : List (Arg Type) → List Term
ArgTypes2Terms = map λ { (arg _ t) → t}

private
  checkDependencies' : List Type → ℕ → ℕ → List ℕ
  checkDependencies' [] i n = []
  checkDependencies' (x ∷ ts) zero n = if occurs0 x then n ∷ [] else []
  checkDependencies' (x ∷ ts) (suc i) n = if occursN (suc i) x then n ∷ checkDependencies' ts i (suc n) else (checkDependencies' ts i (suc n))

checkDependencies : List Type → ℕ → List ℕ
checkDependencies ts zero = []
checkDependencies ts (suc n) = checkDependencies' ts n zero

private
  listContains : ℕ → List ℕ → Bool
  listContains n = foldr (λ x acc → eqNat n x || acc) false

listsIntersect : List ℕ → List ℕ → Bool
listsIntersect xs = foldr (λ n acc → listContains n xs || acc) false

private
  Subst' : Set → Set
  Subst' a = ℕ → ℕ → a → a
  
  substTerm' : Subst' Term
  substArgs' : Subst' (List (Arg Term))
  substArg' : Subst' (Arg Term)
  substAbs' : Subst' (Abs Term)
  substSort' : Subst' Sort
  substClauses' : Subst' (List Clause)
  substClause' : Subst' Clause
  
  substTerm' n σ (var x args) = if eqNat σ x then var (pred n) (substArgs' n σ args) else var x (substArgs' n σ args)
  substTerm' n σ (con c args) = con c (substArgs' n σ args)
  substTerm' n σ (def f args) = def f (substArgs' n σ args)
  substTerm' n σ (meta x args) = meta x (substArgs' n σ args)
  substTerm' n σ (lam v b) = lam v (substAbs' n σ b)
  substTerm' n σ (pat-lam cs args) = pat-lam (substClauses' n σ cs) (substArgs' n σ args)
  substTerm' n σ (pi a b) = pi (substArg' n σ a) (substAbs' n σ b)
  substTerm' n σ (agda-sort s) = agda-sort (substSort' n σ s)
  substTerm' n σ (lit l) = lit l
  substTerm' n σ unknown = unknown
  
  substSort' n σ (set t) = set (substTerm' n σ t)
  substSort' _ σ (lit n) = lit n
  substSort' _ σ unknown = unknown
  
  substClauses' n σ [] = []
  substClauses' n σ (c ∷ cs) = substClause' n σ c ∷ substClauses' n σ cs
  
  substClause' n σ (clause ps b) = clause ps (substTerm' (n + m) (σ + m) b) where m = patternArgsVars ps
  substClause' n σ (absurd-clause ps) = absurd-clause ps
  
  substArgs' n σ [] = []
  substArgs' n σ (x ∷ args) = substArg' n σ x ∷ substArgs' n σ args
  substArg' n σ (arg i x) = arg i (substTerm' n σ x)
  substAbs' n σ (abs x v) = abs x $ substTerm' (suc n) (suc σ) v

substTerm0 : ℕ → Term → Term
substTerm0 = substTerm' zero
