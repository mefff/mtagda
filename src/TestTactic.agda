{-# OPTIONS -v test:2 #-}

module TestTactic where

open import Prelude.Function
open import Prelude.Unit
open import Prelude.List
open import Prelude.Nat renaming (Nat to ℕ)
open import Tactic.Reflection renaming (returnTC to return ; bindTC to _>>=_ ; unify to unifyTC) hiding (Tactic)

open import Agda.Primitive

open import Helpers
open import M
open import Tactic

data _≤_ : ℕ → ℕ → Set where
  0≤ : {n : ℕ} → 0 ≤ n
  s≤ : {n m : ℕ} → n ≤ m → (suc n) ≤ (suc m)

macro
  test-exact : Term → TC ⊤
  test-exact hole = do
    v ← mrun evar
    _ ← mrun (exact 0 (metavar gsOpen ℕ v))
    t ← quoteTC $ unlift v
    unifyTC hole t

  test-exact-2 : Term → TC ⊤
  test-exact-2 hole = do
    lift t ← mrun $ evar {A = Lift (lsuc lzero) ℕ} >>=' λ v → exact 0 (metavar gsOpen ℕ v) >>=' λ _ → ret v
    u ← quoteTC t
    unifyTC hole u

  test-intro : Term → TC ⊤
  test-intro hole = do
    lift t ← mrun $ evar {A = Lift (lsuc lzero) (ℕ → ℕ)} >>=' λ v → intro {A = ℕ} "n"
      (λ x g → evar {A = Lift (lsuc lzero) ℕ} >>=' λ v → exact x g)
      (metavar gsOpen (ℕ → ℕ) v) >>=' λ _ → ret v
    u ← quoteTC t
    unifyTC hole u

  test-intro-2 : Term → TC ⊤
  test-intro-2 hole = do
    lift t ← mrun $ evar {A = Lift (lsuc lzero) ((n : ℕ) → 0 ≤ n)} >>=' λ v → intro {A = ℕ} "n"
      (λ x g → evar {A = Lift (lsuc lzero) (0 ≤ x)} >>=' λ v → exact (0≤ {x}) g)
      (metavar gsOpen ((n : ℕ) → 0 ≤ n) v) >>=' λ _ → ret v
    u ← quoteTC t
    unifyTC hole u

  test-intro-3 : Term → TC ⊤
  test-intro-3 hole = do
    _ ← debugPrint "test" 2 [ strErr "-----test-intro-3-----" ]
    lift t ← mrun $ evar {A = Lift (lsuc lzero) ((n : ℕ) → (m : ℕ) → n ≤ m → suc n ≤ suc m)} >>=' λ v →
               intro {A = ℕ} "n"
                 (λ x g1 →
                 intro {A = ℕ} "m"
                   (λ y g2 →
                   intro {A = x ≤ y} "n≤m"
                     (λ z g3 →
                        exact (s≤ {x} {y} z) g3)
                     g2
                   )
                   g1
                  )
                 (metavar gsOpen ((n : ℕ) → (m : ℕ) → n ≤ m → (suc n) ≤ (suc m)) v) >>=' λ _ → ret v
    u ← quoteTC t
    unifyTC hole u

theorem : Set
theorem = (n m : ℕ) → n ≤ m → suc n ≤ suc m

proof₁ : theorem
proof₁ = test-intro-3

proof₂ : theorem
proof₂ = λ _ _ → s≤
