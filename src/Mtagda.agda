{-# OPTIONS -v test:2 #-}

module Mtagda where

open import Prelude.Function
open import Prelude.String
open import Prelude.Bool
open import Prelude.Unit
open import Prelude.Empty
open import Prelude.List
open import Prelude.Maybe
open import Agda.Builtin.Nat renaming (Nat to ℕ)
open import Tactic.Reflection renaming (bindTC to _>>=_ ; unify to unifyTC)
open import Tactic.Reflection.Free
open import Agda.Primitive

open import M
open import Helpers

macro
  testBeta : (A : ℕ) → Term → TC ⊤
  testBeta A hole = do
    t ← mrun $ abs-fun {A = ℕ} {P = λ _ → ℕ} A A "x"
    u ← quoteTC t
    unifyTC hole u

Type' : ∀ {ℓ} → {A : Set ℓ} → Lift (lsuc lzero) A → Set
Type' (lift x) = ℕ

macro
  test998 : (A : Set) → (Lift (lsuc lzero) A) → Term → TC ⊤
  test998 A x hole = do
    t ← mrun $ abs-fun x (dyn A (unlift x)) "y"
    u ← quoteTC t
    unifyTC hole u

  -- normalise λ A x (t : Type' x) → test998 A x should work

macro
  test999 : (A : Set) → (Lift (lsuc lzero) A) → Term → TC ⊤
  test999 A x hole = do
    t ← mrun $ abs-fun A (dyn A (unlift x)) "A"
    u ← quoteTC t
    unifyTC hole u

test0 : TC ((y : ℕ) → ℕ → ℕ)
test0 = mrun $ nu λ v → abs-fun v (λ t → (λ u → t + u + v) t) "v"

macro
  test00 : Term → TC ⊤
  test00 hole = do
    t ← test0
    u ← quoteTC t
    unifyTC hole u

macro
  test000 : Term → TC ⊤
  test000 hole = do
    debugPrint "test" 2 [ strErr "-------------" ]
    t ← mrun $ nu {A = Set} (λ A →
               nu {A = Lift (lsuc lzero) A} (λ x →
               nu {A = Lift (lsuc lzero) (Type' x)} (λ y →
               abs-fun x (dyn A (unlift x)) "x" >>=' λ r → abs-fun x r "φ" >>=' λ r → abs-fun A r "A")))
    u ← quoteTC t
    unifyTC hole u

_ : (A : Set) → Lift (lsuc lzero) A → Lift (lsuc lzero) A → Dyn
_ = test000

macro
  fail : Term → TC ⊤
  fail hole = do
    st ← quoteTC Set
    t ← extendContext (vArg st) (do
      v ← unquoteTC {A = Set} $ var₀ zero
      extendContext (vArg (var₀ zero)) (do
        unquoteTC {A = v} $ var₀ zero
        returnTC tt
        )
      )
    u ← quoteTC {A = ⊤} t
    unifyTC hole u

macro
  bot : Term → TC ⊤
  bot hole = do
    t ← mrun (mfix1 (λ (f : ⊤ → M ⊥) → f) tt) -- t : ⊥
    u ← quoteTC t
    unifyTC hole u

test3 : (ℕ → M ℕ) → (ℕ → M ℕ)
test3 f zero = ret zero
test3 f (suc n) = f n

test4 : TC ℕ
test4 = mrun (mfix1 test3 $ suc $ suc $ suc $ suc $ suc $ suc $ suc zero)

macro
    test5 : Term → TC ⊤
    test5 hole = do
      catchTC (do
        x ← quoteTC zero
        unifyTC hole x
        )
        (do
        x ← quoteTC {_} {⊤} tt
        unifyTC hole x
        )

test6 : Term
test6 = con (quote just) (hArg (quoteTerm lzero) ∷ (hArg (quoteTerm ℕ)) ∷ [ vArg (con₀ (quote zero)) ])

macro
  test7 : Term → TC ⊤
  test7 hole = do
    unifyTC test6 hole

test8 : Maybe ℕ
test8 = test7

macro
  test9 : Term → TC ⊤
  test9 hole = do
    t ← mrun $ (evar >>=' (λ e → catch (unify e zero >>=' (λ _ → raise e)) (ret e)))
    u ← quoteTC t
    unifyTC hole u

macro
  test10 : Term → TC ⊤
  test10 hole = do
    t ← mrun $ (catch (evar >>=' (λ e → unify e zero >>=' λ _ → raise e)) (ret zero))
    u ← quoteTC t
    unifyTC hole u

macro
  test11 : Term → TC ⊤
  test11 hole = do
    extendContext (vArg (quoteTerm ℕ)) (do
      unifyTC hole (var zero []))

isVar0 : Term → Bool
isVar0 (var₀ zero) = true
isVar0 (var zero (x ∷ args)) = false
isVar0 (var (suc x) args) = false
isVar0 (con c args) = false
isVar0 (def f args) = false
isVar0 (lam v t) = false
isVar0 (pat-lam cs args) = false
isVar0 (pi a b) = false
isVar0 (agda-sort s) = false
isVar0 (lit l) = false
isVar0 (meta x x₁) = false
isVar0 unknown = false

macro
  test12 : Term → TC ⊤
  test12 hole = do
    x ← extendContext (vArg $ quoteTerm ℕ) (do
      let t = var zero []
      if (occurs0 t) then (mrun $ raise t) else (returnTC $ quoteTerm zero))
    tr ← quoteTC {A = Bool} true
    fl ← quoteTC {A = Bool} false
    if isVar0 x then unifyTC hole tr else unifyTC hole fl
