{-# OPTIONS -v test:2 #-}

module M where

open import Agda.Primitive
open import Prelude.String
open import Prelude.Maybe
open import Prelude.List
open import Prelude.Function
open import Prelude.Equality
open import Prelude.Nat renaming (Nat to ℕ)
open import Prelude.Bool
open import Prelude.Unit
open import Tactic.Reflection renaming (bindTC to _>>=_ ; unify to unifyTC)

open import Helpers

_>>_ : ∀ {ℓ₁ ℓ₂} {A : Set ℓ₁} {B : Set ℓ₂} → TC A → TC B → TC B
a >> b = a >>= (λ _ → b)


data Lift {ℓ} ℓ' : Set ℓ → Set (ℓ ⊔ ℓ') where
  lift : {A : Set ℓ} → A → Lift ℓ' A

unlift : ∀ {ℓ ℓ'} {A : Set ℓ} → Lift ℓ' A → A
unlift (lift x) = x

lmap : ∀ {ℓ ℓ'} {A : Set ℓ} {B : A → Set ℓ} → ((x : A) → B x) → (x : Lift ℓ' A) → Lift ℓ' (B $ unlift x)
lmap f x = lift $ f $ unlift x

⟪_⟫ : ∀ {ℓ ℓ'} {A : Set ℓ} {B : A → Set ℓ} → ((a : Lift ℓ' A) → Lift ℓ' (B (unlift a))) → (x : A) → B x
⟪ f ⟫ = unlift ∘ f ∘ lift

{-# NO_POSITIVITY_CHECK #-}
data M {ℓ} : Set ℓ → Set (lsuc ℓ) where
  ret : {A : Set ℓ} → A → M A
  bind : {A B : Set ℓ} → M A → (A → M B) → M B
  mfix1 : {A : Set ℓ} {B : A → Set ℓ} → (((x : A) → M (B x)) → ((x : A) → M (B x))) → (x : A)
          → M (B x)
  evar : {A : Set ℓ} → M A
  unify : {A : Set ℓ} → (x y : A) → M (Maybe (x ≡ y))
  raise : {A B : Set ℓ} → A → M B
  catch : {A : Set ℓ} → M A → M A → M A
  nu : {A B : Set ℓ} → (A → M B) → M B
  abs-fun : {A : Set ℓ} {P : A → Set ℓ} → (x : A) → P x → String →
            M ((y : A) → P y)
  --abs_prod : {A : Set ℓ} → (x : A) → Set ℓ → M (Set (lsuc ℓ))

  mquote : {A : Set ℓ} → A → M $ Lift ℓ Term
  debug : {A : Set ℓ} → String → A → M $ Lift ℓ ⊤

_>>='_ : ∀ {ℓ} {A B : Set ℓ} → M A → (A → M B) → M B
_>>='_ = bind

{-# TERMINATING #-}
mrun : ∀ {ℓ} {A : Set ℓ} → M A → TC A
mrun (ret x) = returnTC x
mrun (bind x f) = do
  a ← mrun x
  mrun $ f a
mrun (mfix1 f x) = do
  mrun $ f (mfix1 f) x
mrun (evar {S}) = do
  t ← quoteTC S
  m ← checkType unknown t
  unquoteTC m
mrun {ℓ} (unify {A} x y) = do
  tx ← quoteTC x
  ty ← quoteTC y
  tℓ ← quoteTC ℓ
  tA ← quoteTC A
  tx≡y ← quoteTC $ x ≡ y
  catchTC (do
    unifyTC tx ty
    unquoteTC
      (con (quote just)
        (hArg tℓ ∷ hArg tx≡y ∷ [ vArg (con (quote refl) (hArg tℓ ∷ hArg tA ∷ [ hArg tx ])) ]))
    )
    (unquoteTC $ con (quote nothing) (hArg tℓ ∷ [ hArg tx≡y ]))
mrun (raise x) = do
  t ← quoteTC x
  typeError [ termErr t ]
mrun (catch t u) = do
  catchTC (mrun t) (mrun u)
mrun {A = B} (nu {A = A} f) = do
  debugPrint "test" 2 [ strErr "nu" ]
  a ← quoteTC A
  debugPrint "test" 2 $ strErr "A:" ∷ termErr a ∷ []
  t ← extendContext (vArg a) (do
    v ← unquoteTC $ var₀ zero
    debugPrint "test" 2 $ strErr "Extending context" ∷ []
    mrun $ f v)
  u ← quoteTC t
  debugPrint "test" 2 $ strErr "t:" ∷ termErr u ∷ []
  if occurs0 u then mrun $ raise t else (do
    let u' = updateTerm Pred u
    t' ← unquoteTC u'
    mrun $ ret t'
    )
mrun (abs-fun {A} {P} x t str) = do
  debugPrint "test" 2 [ strErr "abs-fun" ]
  var₀ n ← quoteTC x
    where err → mrun $ raise $ lift err
  quoteTC {A = Term} (var₀ n) >>= λ z → debugPrint "test" 2 $ strErr "n:" ∷ termErr z ∷ []
  u ← quoteTC t
  debugPrint "test" 2 $ strErr "t:" ∷ termErr u ∷ []
  c ← getContext
  let ctx = ArgTypes2Terms c
  let deps = checkDependencies ctx n
  d' ← quoteTC deps
  d'' ← normalise d'
  d''' ← unquoteTC {A = List ℕ} d''
  debugPrint "test" 2 $ strErr "deps" ∷ termErr d'' ∷ []
  if listsIntersect deps (freeVars u) then mrun $ raise (lift d''') else (do
    debugPrint "test" 2 $ strErr "Creating λ" ∷ []
    let u' = lam visible $ abs str $ updateTerm Suc u
    debugPrint "test" 2 $ termErr u' ∷ []
    let u' = substTerm0 n u'
    debugPrint "test" 2 $ termErr u' ∷ []
    unquoteTC u'
    )
mrun {ℓ} (mquote x) = do
  t ← quoteTC x
  returnTC (lift t)
mrun (debug str x) = do
  t ← quoteTC x
  debugPrint "test" 2 $ strErr str ∷ strErr " | " ∷ termErr t ∷ []
  returnTC $ lift tt

mmap : ∀ {ℓ} {A B : Set ℓ} → (A → M B) → List A → M $ List B
mmap f [] = ret []
mmap f (x ∷ xs) = f x >>=' λ t → mmap f xs >>=' λ ts → ret (t ∷ ts)

data Dyn {ℓ} : Set (lsuc ℓ) where
  dyn : (A : Set ℓ) → A → Dyn
