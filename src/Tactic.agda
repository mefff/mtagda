{-# OPTIONS -v test:2 #-}

module Tactic where

open import Agda.Primitive

open import Prelude.List
open import Prelude.Function
open import Prelude.Equality
open import Prelude.Maybe
open import Prelude.String
open import Prelude.Unit

open import Helpers
open import M renaming (ret to return ; bind to _>>=_)

data GoalState : Set where
  gsOpen gsAny : GoalState

data Goal {ℓ} : GoalState → Set (lsuc ℓ) where
  metavar : (gs : GoalState) (A : Set ℓ) → Lift (lsuc ℓ) A → Goal gs
  hyp : {A : Set ℓ} → (Lift (lsuc ℓ) A → Goal {ℓ} gsAny) → Goal gsAny

Tactic : ∀ {ℓ} → Set _
Tactic {ℓ} = Goal {ℓ} gsOpen → M $ List $ Goal {ℓ} gsAny

idtac : ∀ {ℓ} → Tactic {ℓ}
idtac (metavar .gsOpen A x) = return [ metavar gsAny A x ]

try : ∀ {ℓ} → Tactic {ℓ} → Tactic
try t g = catch (t g) (idtac g)

exact : ∀ {ℓ} {A : Set ℓ} → A → Tactic {ℓ}
exact {ℓ} {A} x (metavar .gsOpen B y) = do
  just refl ← unify A B
    where nothing → raise A
  just refl ← unify (lift x) y
    where nothing → raise $ lift x
  _ ← debug {A = Lift (lsuc ℓ) A} "term" y
  return []

close : ∀ {ℓ} {B : Set ℓ} → Lift (lsuc ℓ) B → List $ Goal {ℓ} gsAny → M $ List $ Goal {ℓ} gsAny
close {ℓ} y = mmap λ g → do
  r ← abs-fun y g "y"
  return (hyp r)

intro : ∀ {ℓ} {A : Set ℓ} → String → (A → Tactic {ℓ}) → Tactic {ℓ}
intro {ℓ} {A} str f (metavar .gsOpen B y) = do
  P ← evar {A = A → Set ℓ}
  e ← evar {A = Lift (lsuc ℓ) ((x : A) → P x)}
  just refl ← unify ((x : A) → P x) B
    where nothing → raise B
  just refl ← unify e y
    where nothing → raise y
  nu {A = Lift (lsuc ℓ) A} λ x → do
    e' ← evar {A = Lift (lsuc ℓ) $ P $ unlift x}
    _ ← debug {A = Lift (lsuc ℓ) ⊤} str (lift tt)
    ng ← abs-fun {A = Lift (lsuc ℓ) A} {P = λ z → Lift (lsuc ℓ) (P $ unlift z)} x e' str
    _ ← exact {A = (x : A) → P x} ⟪ ng ⟫ (metavar gsOpen B y)
    _ ← debug {A = Lift (lsuc ℓ) ((x : A) → P x)} "term before f" y
    r ← f (unlift x) (metavar gsOpen (P (unlift x)) (ng x)) >>= close x
    _ ← debug {A = Lift (lsuc ℓ) ((x : A) → P x)} "term after f" y
    return r

open∧apply : ∀ {ℓ} → Tactic {ℓ} → Goal {ℓ} gsAny → M $ List $ Goal {ℓ} gsAny
open∧apply t (metavar .gsAny A x) = t (metavar gsOpen A x)
open∧apply t (hyp f) = nu λ x → open∧apply t (f x) >>=' close x

apply : ∀ {ℓ} {A : Set ℓ} → A → Tactic {ℓ}
apply {ℓ} {A} t (metavar .gsOpen B x) = do
  mfix1 mfixApply (dyn A t)
  where
  mfixApply : (Dyn {ℓ} → M (List (Goal {ℓ} gsAny))) → Dyn {ℓ} → M (List (Goal {ℓ} gsAny))
  mfixApply go (dyn A f) = do
    unify A B >>=
      λ where
        (just refl) → return []
        nothing → do
          T1 ← evar {A = Set ℓ}
          T2 ← evar {A = Set ℓ}
          unify (T1 → T2) A >>=
            λ where
              (just refl) → do
                e ← evar {A = Lift (lsuc ℓ) T1}
                r ← go (dyn T2 (f $ unlift e))
                return (metavar gsAny T1 e ∷ r)
              nothing → do
                T1 ← evar {A = Set ℓ}
                P ← evar {A = T1 → Set ℓ}
                unify ((x : T1) → P x) A >>=
                  λ where
                    (just refl) → do
                      lift e ← evar {A = Lift (lsuc ℓ) T1}
                      r ← go (dyn (P e) (f e))
                      return r
                    nothing → raise A
